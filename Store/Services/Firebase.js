import * as firebase from 'firebase';

var config = {
  apiKey: "AIzaSyD4rbIpyx0Qh3HaAgvR7DGYZyYh5ptKwXE",
  authDomain: "clone-instagram-5ce92.firebaseapp.com",
  databaseURL: "https://clone-instagram-5ce92.firebaseio.com",
  projectId: "clone-instagram-5ce92",
  storageBucket: "clone-instagram-5ce92.appspot.com",
  messagingSenderId: "282440268706"
};

firebase.initializeApp(config);

export const autentication = firebase.auth();
export const baseDados = firebase.database();

