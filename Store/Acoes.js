import constantes from './Constantes';

export const actionRegistro = values => ({
  type: constantes.REGISTRO,
  dados: values,
});

export const actionLogin = dados => ({
  type: constantes.LOGIN,
  dados,
});

export const actionEstabeleceSession = usuario => ({
  type: constantes.ESTABELECER_SESSION,
  usuario,
});

export const actionEncerrarSession = () => ({
  type: constantes.ENCERRAR_SESSION,
});

export const actionCargaImagem = imagem => ({
  type: constantes.CARGA_IMAGEM_SIGNUP,
  imagem,
});

export const actionLimparImagem = () => ({
  type: constantes.IMAGEM_LIMPA_SIGNUP,
});

export const actionCargaPublicacao = imagem => ({
  type: constantes.CARGA_IMAGEM_PUBLICACAO,
  imagem,
});

export const actionLimparPublicacao = () => ({
  type: constantes.IMAGEM_LIMPA_PUBLICACAO,
});

export const actionSubirPublicacao = values => ({
  type: constantes.SUBIR_PUBLICACAO,
  values,
});

export const actionConsultaPublicacao = () => ({
  type: constantes.CONSULTA_PUBLICACAO,
});

export const actionAgregarPublicacaoStore = publicacoes => ({
  type: constantes.AGREGAR_PUBLICACAO_STORE,
  publicacoes,
});

export const actionAgregarAutoresStore = autores => ({
  type: constantes.AGREGAR_AUTORES_STORE,
  autores,
});

export const actionExitoSubirPublicacao = () => ({
  type: constantes.EXITO_SUBIR_PUBLICACAO,
});

export const actionErrorSubirPublicacao = () => ({
  type: constantes.ERROR_SUBIR_PUBLICACAO,
});

export const actionLimparSubirPublicacao = () => ({
  type: constantes.LIMPAR_SUBIR_PUBLICACAO,
});