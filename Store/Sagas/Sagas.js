import { takeEvery, call, select, put, all } from 'redux-saga/effects';
import { autentication, baseDados } from '../Services/Firebase';
import Constantes from '../Constantes';
import { actionAgregarPublicacaoStore, actionAgregarAutoresStore, actionExitoSubirPublicacao, actionErrorSubirPublicacao } from '../Acoes';
import axios from 'axios';

const registroEmFirebase = values => autentication
  .createUserWithEmailAndPassword(values.email, values.password)
  .then(success => success.toJSON());

const registroEmBaseDados = ({ uid, email, nome, fotoURL }) =>
  baseDados.ref(`usuarios/${uid}`).set({
    nome, email, fotoURL
  });

const registroFotoCloudinary = ({ imagem }) => {
  console.log(imagem);
  const { uri, type } = imagem;
  const splitName = uri.split('/');
  const name = [...splitName].pop();
  const foto = {
    uri,
    type,
    name,
  };
  const formImagem = new FormData();
  formImagem.append('upload_preset', Constantes.CLOUDINARY_PRESET);
  formImagem.append('file', foto);

  //return axios.post(Constantes.CLOUDINARY_NAME, formImagem)
  //.then((result) => {
  //    console.log(result);
  //})
  //.catch((err) => {
  //    console.log(err);
  //});
 

  return fetch(Constantes.CLOUDINARY_NAME, {
    method: 'POST',
    body: formImagem,
  }).then(response => response.json());
};

const loginFirebase = ({ email, password }) =>
  autentication.signInWithEmailAndPassword(email, password)
    .then(success => success.toJSON());

function* sagaLogin(values) {
  try {
    const resultado = yield call(loginFirebase, values.dados);
  } catch (error) {
    console.log(error);
  }
}

function* sagaRegistro(values) {
  try {
    const imagem = yield select(state => state.reducerImagemSignUp);
    const urlFoto = yield call(registroFotoCloudinary, imagem);
    const fotoURL = urlFoto.secure_url;
    console.log(urlFoto);
    const registro = yield call(registroEmFirebase, values.dados);
    const { email, uid } = registro;
    const { dados: { nome } } = values;
    yield call(registroEmBaseDados, {
      uid,
      email,
      nome,
      fotoURL,
    });
  } catch (error) {
    console.log(error);
  }
}

const escreverFirebase = ({ width, height, secure_url, uid }, texto = "") =>
  baseDados.ref(`publicacoes/`).push({
    width,
    height,
    secure_url,
    uid,
    texto,
  }).then(response => response);

  const escreverAutorPublicacoes = ({ uid, key }) => baseDados.ref(`autor-publicacoes/${uid}`)
  .update({ [key]: true})
  .then(response => response);

function* sagaSubirPublicacao({ values }) {
  try {
    const imagem = yield select(state => state.reducerImagemPublicacao);
    const usuario = yield select(state => state.reducerSession);
    const { uid } = usuario;
    const resultadoImagem = yield call(registroFotoCloudinary, imagem);
    const { width, height, secure_url } = resultadoImagem;
    const parametrosImagem = { width, height, secure_url, uid };
    const escreverEmFirebase = yield call(escreverFirebase, parametrosImagem, values.texto);
    const { key } = escreverEmFirebase;
    const parametrosAutorPublicacoes = { uid, key };
    const resultEscreverAutorPublicacoes = yield call(escreverAutorPublicacoes, parametrosAutorPublicacoes);
    yield put(actionExitoSubirPublicacao());

  } catch (error) {
    console.log(error);
    yield put(actionErrorSubirPublicacao());
  }
}

const consultaPublicacoes = () => 
baseDados.ref(`publicacoes/`)
  .once('value')
  .then(snapshot => {
    const publicacoes = [];
      snapshot.forEach((childSnapshot) => {
      const { key } = childSnapshot;
      const publicacao = childSnapshot.val();
      publicacao.key = key;
      publicacoes.push(publicacao);
  });
  return publicacoes;
}
);

const consultaAutor = (uid) =>
baseDados.ref(`usuarios/${uid}`)
.once('value')
.then(snapshot=> snapshot.val());

function* sagaConsultaPublicacao({ values }) {
  try {
    const publicacoes = yield call(consultaPublicacoes);
    const autores = yield all(publicacoes.map(publicacao => call(consultaAutor, publicacao.uid)));
  
    yield put(actionAgregarAutoresStore(autores));
    yield put(actionAgregarPublicacaoStore(publicacoes));
  } catch (error) {
    console.log(error);
  }
}

export default function* functionPrimaria() {
  yield takeEvery(Constantes.REGISTRO, sagaRegistro);
  yield takeEvery(Constantes.LOGIN, sagaLogin);
  yield takeEvery(Constantes.SUBIR_PUBLICACAO, sagaSubirPublicacao);
  yield takeEvery(Constantes.CONSULTA_PUBLICACAO, sagaConsultaPublicacao);
}
