import { createStore, combineReducers, applyMiddleware } from 'redux';
import { reducer as form } from 'redux-form';
import createSagaMiddleware from 'redux-saga';
import functionPrimaria from './Sagas/Sagas';
import Constantes from './Constantes';

const reducerPrueba = (state = [0], action) => {
  switch (action.type) {
    case 'AUMENTAR_REDUCER_PRUEBA':
      return [state, 1];
    default:
      return state;
  }
};

const reducerSession = (state = null, action) => {
  switch (action.type) {
    case Constantes.ESTABELECER_SESSION:
      return action.usuario;
    case Constantes.ENCERRAR_SESSION:
      return null;
    default:
      return state;
  }
};

const sagaMiddleware = createSagaMiddleware();

const reducerImagemSignUp = (state = { imagem: null }, action) => {
  switch (action.type) {
    case Constantes.CARGA_IMAGEM_SIGNUP:
      return { imagem: action.imagem };
    case Constantes.IMAGEM_LIMPA_SIGNUP:
      return { imagem: null };
    default:
      return state;
  }
};

const reducerImagemPublicacao = (state = { imagem: null }, action) => {
  switch (action.type) {
    case Constantes.CARGA_IMAGEM_PUBLICACAO:
      return { imagem: action.imagem };
    case Constantes.IMAGEM_LIMPA_PUBLICACAO:
      return { imagem: null };
    default:
      return state;
  }
};

const reducerPublicacoesConsultas = ( state = [], action ) => {
  switch (action.type) {
    case Constantes.AGREGAR_PUBLICACAO_STORE:
      return [...state, ...action.publicacoes];
    default:
      return state
  }
};

const reducerAutoresConsultas = ( state = [], action ) => {
  switch (action.type) {
    case Constantes.AGREGAR_AUTORES_STORE:
      return [...state, ...action.autores];
    default:
      return state
  }
};

const reducerExitoSubirPublicacao = ( state = {estado: null}, action ) => {
  switch (action.type) {
    case Constantes.EXITO_SUBIR_PUBLICACAO:
      return {estado: 'EXITO' };
    case Constantes.ERROR_SUBIR_PUBLICACAO:
      return {estado: 'ERROR' };
    case Constantes.LIMPAR_SUBIR_PUBLICACAO:
      return {estado: null };
    default:
      return state;
  }
};

const reducers = combineReducers({
  reducerExitoSubirPublicacao,
  reducerAutoresConsultas,
  reducerPublicacoesConsultas,
  reducerImagemPublicacao,
  reducerImagemSignUp,
  reducerSession,
  reducerPrueba,
  form,
});

const store = createStore(reducers, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(functionPrimaria);

export default store;
