import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Provider } from 'react-redux';
import RouterNoAuthenticated from './Components/NoAuthenticated/RouterNoAuthenticated';
import RouterAuthenticated from './Components/Authenticated/RouterAuthenticated';
import Store from './Store/Store';
import Selecao from './Selecao';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = { nome: 'Aluguel Casa/Apto' };
  }

  render() {
    return (
      <View style={styles.container}>
        <Provider store={Store}>
          <Selecao />
        </Provider>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
});
