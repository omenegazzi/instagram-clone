import { StackNavigator } from 'react-navigation';
import Add from './Add';
import SelecionarGaleria from './SelecionarGaleria';

const StackAdd = StackNavigator({
  Add: {
    screen: Add,
  },
  Selection: {
    screen: SelecionarGaleria,
  },
  
});

export default StackAdd;

