import React, { Component } from 'react';
import { StyleSheet, View, Text, Button, Dimensions, Image } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

class Publication extends Component {
  constructor() {
    super();
    this.state = { nome: 'instagram-clone' };
  }

  render() {
    const { navigation, item, autor} = this.props;
    const { width } = Dimensions.get('window');
    const factor = item.width / width;
    const height = item.height / factor;
                
    return (
      <View>
        <View style={styles.header}>
          <Image source={{uri: autor.fotoURL }} style={{width: 30, height: 30, borderRadius: 24}}/>
          <Text></Text>
          <Text>{autor.nome}</Text>
        </View>
        <Image source={{ uri: item.secure_url }}
              style={{width, height}} /> 
        <View style={styles.footer}>
        <View style={styles.icons}>
          <Ionicons name='md-heart-outline' color='#000000' size={30} style={styles.icon} />
          <Ionicons name='md-chatboxes' color='#000000' size={30} style={styles.icon}/>
        </View>
        <View style={styles.texto}>
        <Text>{item.texto}</Text>
        </View>
          
          <Text>Comentários</Text>
        </View>


        {/*<Text>Publication</Text>
        <Button title="Autor" onPress={() => { navigation.navigate('Autor'); }} />
        <Button title="Comentários" onPress={() => { navigation.navigate('Comments'); }} >*/ }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#f9f9f9',
    alignItems: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    marginVertical: 16,
  },
  footer: {
    marginHorizontal: 16,
  },
  icons: {
    flexDirection: 'row',
  },
  icon: {
    marginLeft: 16,
    marginVertical: 16,
  },
  texto: {
    marginBottom: 16,
  },
});

export default Publication;
