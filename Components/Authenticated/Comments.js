import React, { Component } from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';

class Comments extends Component {
  static navigationOptions = { tabBarVisible: false };
  constructor() {
    super();
    this.state = { nome: 'instagram-clone' };
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <Text>Comments</Text>
        <Button title="Autor" onPress={() => { navigation.navigate('Autor'); }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2C3E50',
    justifyContent: 'center',
  },
});

export default Comments;
