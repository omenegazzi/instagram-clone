import React, { Component } from 'react';
import { StyleSheet, View, Text, Button, FlatList, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { actionConsultaPublicacao } from '../../Store/Acoes';
import Publication from './Publication';

class Home extends Component {
  componentDidMount(){
    this.props.consultaPublicacoes();
  }
  
  render() {
    console.log(this.props.publicacoes);
    const { navigation, autores } = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.publicacoes}
          renderItem={({ item, index }) => <Publication item={ item } autor={autores[index]} />}
          ItemSeparatorComponent={() => (
            <View style={styles.separador}>
              <Text></Text>
            </View>
          )}
        />
        
        { /*<Text>Home</Text>
        <Button title="Autor" onPress={() => { navigation.navigate('Autor'); }} />
        <Button title="Comentários" onPress={() => { navigation.navigate('Comments'); }} />*/}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f9f9f9',
    justifyContent: 'center',
  },
  separador: {
    borderWidth: 1,
    borderColor: '#C0C0C0',
  }
});

const mapStateToProps = state => ({
  publicacoes: state.reducerPublicacoesConsultas,
  autores: state.reducerAutoresConsultas,
});

const mapDispatchToProps = dispatch => ({
  consultaPublicacoes: () => {
    dispatch(actionConsultaPublicacao());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);