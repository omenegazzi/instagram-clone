import React, { Component } from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';

class Add extends Component {
  constructor() {
    super();
    this.state = { nome: 'instagram-clone' };
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <Button title= "Seleciona galeria" onPress = {() => { navigation.navigate('Selection')}} />
        <Text>Add</Text>
        <Button title= "Tirar Foto" onPress = {() => { navigation.navigate('Selection')}} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#f9f9f9',
    alignItems: 'center',
  },
});

export default Add;
