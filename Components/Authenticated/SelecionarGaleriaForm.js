import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput, Button } from 'react-native';
import { Field, reduxForm } from 'redux-form';

const fieldNome = (props) => {
  return (
    <View style={styles.textInput}>
      <TextInput
        underlineColorAndroid="transparent"
        placeholder={props.ph}
        onChangeText={props.input.onChange}
        value={props.input.value}
        keyboardType="default"
        autoCapitalize="none"
        onBlur={props.input.onBlur}
        multiline
      />
      <View/>
      {props.meta.touched && props.meta.error && <Text style={styles.errros}>{props.meta.error}</Text>}
    </View>
  );
};

const fieldImagem = props => (
  <View>
    <View>
      {props.meta.touched &&
        props.meta.error && <Text style={styles.errros}>{props.meta.error}</Text>}
    </View>
  </View>
);

const validate = (values, props) => {
  const errors = {};
  if (!props.imagem) {
    errors.imagem = 'Imagem é requerida.';
  }
  if (values.texto && values.texto.length > 140) {
    errors.texto = 'Deve ser menor 140 caracteres';
  }

  return errors;
};

const SelecionarGaleriaForm = (props) => {
  return (
    <View style={styles.container}>
      <Field name="imagem" component={fieldImagem} />
      <Field underlineColorAndroid="transparent" name="texto" component={fieldNome} ph="texto" />
      <Button
        title="Registrar"
        onPress={props.handleSubmit(props.registro,
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 3,
  },
  textInput: {
    marginHorizontal: 16,

  },
  errros: {
    color: '#FF0000',
  },
});

export default reduxForm({
  form: 'SelecionarGaleriaForm',
  validate,
})(SelecionarGaleriaForm);
