import { StackNavigator } from 'react-navigation';
import Home from './Home';
import Publication from './Publication';
import Autor from './Profile';
import Comments from './Comments';

const StackHome = StackNavigator({
  Home: {
    screen: Home,
  },
  Autor: {
    screen: Autor,
  },
  Publication: {
    screen: Publication,
  },
  Comments: {
    screen: Comments,
  },
});

export default StackHome;

