import React, { Component } from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';

class Follow extends Component {
  constructor() {
    super();
    this.state = { nome: 'instagram-clone' };
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <Text>Follow</Text>
        <Button title="Autor" onPress={() => { navigation.navigate('Autor'); }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
});

export default Follow;
