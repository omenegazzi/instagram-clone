import React, { Component } from 'react';
import { StyleSheet, View, Text, Button, Alert} from 'react-native';
import SelecionaImagem  from '../SelecionaImagem';
import { connect } from 'react-redux';
import { blur } from 'redux-form';
import { actionCargaPublicacao, actionSubirPublicacao, actionLimparPublicacao } from '../../Store/Acoes';
import SelecionarGaleriaForm from './SelecionarGaleriaForm';

class SelecionarGaleria extends Component {
  constructor() {
    super();
    this.state = { nome: 'instagram-clone' };
  }

  static navigationOptions = {
    tabBarVisible: false,
  };

  componentWillUnmount(){
    this.props.limparImagem();
  };

  componentWillReceiveProps(nextProps){
    if(this.props.estadoSubirPublicacao !== nextProps.estadoSubirPublicacao){
      switch (nextProps.estadoSubirPublicacao) {
        case 'EXITO': 
          Alert.alert('EXITO', 'Publicação realizado com sucesso!', [{text: 'OK', onPress:() => {
          this.props.limparPublicacao();
          }}]);
          this.props.navigation.goBack();
          break;
        case 'ERROR':
        Alert.alert('Erro', 'Ocorreu um erro ao publicar', [{text: 'Confirmar', onPress:() => {
          this.props.limparPublicacao();
          this.props.navigation.goBack();
        }}]);
          break;
        default:
          break;
      }
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.imagem}>
          <SelecionaImagem imagem={this.props.imagem.imagem} carga={this.props.cargaImagem} radius={true}/>
        </View>
        <View style={styles.texto}>
          <SelecionarGaleriaForm 
          imagem={this.props.imagem.imagem} 
          registro={(values) => {
            this.props.subirPublicacao(values)
          }} />
       </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    justifyContent: 'center',
    backgroundColor: '#f9f9f9',
  },
  imagem: {
    flex: 2,
  },
  texto: {
    flex: 2,
  }

});

const mapStateToProps = state => ({
  imagem: state.reducerImagemPublicacao,
  estadoSubirPublicacao: state.reducerExitoSubirPublicacao.estado,
});

const mapDispatchToProps = dispatch => ({
  cargaImagem: (imagem) => {
    dispatch(actionCargaPublicacao(imagem));
    dispatch(blur('SelecionarGaleriaForm', 'imagem', Date.now()));
  },
  subirPublicacao: (values) => {
    dispatch(actionSubirPublicacao(values));
  },
  limparImagem: () => {
    dispatch(actionLimparPublicacao());
  },
  limparPublicacao: () => {
    dispatch(actionLimparPublicacao());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SelecionarGaleria);
