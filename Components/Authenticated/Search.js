import React, { Component } from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';

class Search extends Component {
  constructor() {
    super();
    this.state = { nome: 'instagram-clone' };
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <Text>Search</Text>
        <Button title="Publicação" onPress={() => { navigation.navigate('Publication'); }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2C3E50',
    justifyContent: 'center',
  },
});

export default Search;
