import { StackNavigator } from 'react-navigation';
import Search from './Search';
import Publication from './Publication';
import Autor from './Profile';
import Comments from './Comments';

const StackSearch = StackNavigator({
  Search: {
    screen: Search,
  },
  Autor: {
    screen: Autor,
  },
  Publication: {
    screen: Publication,
  },
  Comments: {
    screen: Comments,
  },
});

export default StackSearch;

