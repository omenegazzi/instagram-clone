import { StackNavigator } from 'react-navigation';
import TabFollow from './TabFollow';
import Autor from './Profile';
import Publication from './Publication';
import Comments from './Comments';

const StackFollow = StackNavigator({
  TabFollow: {
    screen: TabFollow,
    navigationOptions: {
      header: null,
    },
  },
  Autor: {
    screen: Autor,
  },
  Publication: {
    screen: Publication,
  },
  Comments: {
    screen: Comments,
  },
});

export default StackFollow;
