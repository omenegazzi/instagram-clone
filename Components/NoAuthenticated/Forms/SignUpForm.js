import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput, Button } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { autentication } from '../../../Store/Services/Firebase';

const fieldNome = (props) => {
  return (
    <View style={styles.textInput}>
      <TextInput
        underlineColorAndroid="transparent"
        placeholder={props.ph}
        onChangeText={props.input.onChange}
        value={props.input.value}
        keyboardType={props.input.name === 'email' ? 'email-address' : 'default'}
        autoCapitalize="none"
        secureTextEntry={!!(props.input.name === 'password' || props.input.name === 'confirm')}
        onBlur={props.input.onBlur}
      />
      <View style={styles.linea} />
      {props.meta.touched && props.meta.error && <Text style={styles.errros}>{props.meta.error}</Text>}
    </View>
  );
};

const fieldImagem = props => (
  <View>
    <View>
      {props.meta.touched &&
        props.meta.error && <Text style={styles.errros}>{props.meta.error}</Text>}
    </View>
  </View>
);

const validate = (values, props) => {
  const errors = {};
  if (!props.imagem) {
    errors.imagem = 'Imagem é requerida.';
  }
  if (!values.nome) {
    errors.nome = 'Requerido';
  } else if (values.nome.length < 5) {
    errors.nome = 'Devem ser ter ao menos 5 caracteres';
  } else if (values.nome.length > 10) {
    errors.nome = 'Deve ser menor 10 caracteres';
  }

  if (!values.email) {
    errors.email = 'Requerido';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'E-mail inválido!';
  }

  if (!values.password) {
    errors.password = 'Requerido';
  } else if (values.password.length < 5) {
    errors.password = 'Devem ser ter ao menos 5 caracteres';
  } else if (values.password.length > 15) {
    errors.password = 'Deve ser menor 15 caracteres';
  }

  if (!values.confirm) {
    errors.confirm = 'Requerido';
  } else if (values.password !== values.confirm) {
    errors.confirm = 'senha não confere!';
  }

  return errors;
};

const SignUpForm = (props) => {
  return (
    <View style={styles.container}>
      <Field name="imagem" component={fieldImagem} />
      <Field underlineColorAndroid="transparent" name="nome" component={fieldNome} ph="nome" />
      <Field underlineColorAndroid="transparent" name="email" component={fieldNome} ph="email@email.com" />
      <Field underlineColorAndroid="transparent" name="password" component={fieldNome} ph="*******" />
      <Field underlineColorAndroid="transparent" name="confirm" component={fieldNome} ph="******* " />
      <Button
        title="Registrar"
        onPress={props.handleSubmit(props.registro,
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 3,
  },
  textInput: {
    marginBottom: 16,
  },
  linea: {
    backgroundColor: '#000',
    height: 2,
  },
  errros: {
    color: '#FF0000',
  },
});

export default reduxForm({
  form: 'SignUpForm',
  validate,
})(SignUpForm);
