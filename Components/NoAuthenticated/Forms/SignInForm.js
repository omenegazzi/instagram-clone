import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput, Button } from 'react-native';
import { Field, reduxForm } from 'redux-form';

const fieldNome = (props) => {
  console.log('props');
  return (
    <View>
      <TextInput
        placeholder={props.ph}
        onChangeText={props.input.onChange}
        value={props.input.value}
        keyboardType={props.input.name === 'email' ? 'email-address' : 'default'}
        autoCapitalize="none"
        secureTextEntry={!!(props.input.name === 'password' || props.input.name === 'confirm')}
        onBlur={props.input.onBlur}
      />
      <View style={styles.linea} />
      {props.meta.touched && props.meta.error && <Text style={styles.errros}>{props.meta.error}</Text>}
    </View>
  );
};

const validate = (values) => {
  const errors = {};

  if (!values.email) {
    errors.email = 'Requerido';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'E-mail inválido!';
  }

  if (!values.password) {
    errors.password = 'Requerido';
  } else if (values.password.length < 5) {
    errors.password = 'Devem ser ter ao menos 5 caracteres';
  } else if (values.password.length > 15) {
    errors.password = 'Deve ser menor 15 caracteres';
  }

  return errors;
};


const SignInForm = (props) => {
  return (
    <View>
      <Field name="email" component={fieldNome} ph="email@email.com" />
      <Field name="password" component={fieldNome} ph="*******" />      
      <Button
        title="Login"
        onPress={props.handleSubmit(props.login)} />
    </View>
  );
};

const styles = StyleSheet.create({
  textInput: {
    marginBottom: 16,
  },
  linea: {
    backgroundColor: '#000',
    height: 2,
  },
  errros: {
    color: '#FF0000',
  },
});

export default reduxForm({
  form: 'SignInForm',
  validate,
})(SignInForm);
