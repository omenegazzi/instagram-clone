import React, { Component } from 'react';
import { StyleSheet, View, Button } from 'react-native';
import SignInForm from './Forms/SignInForm';
import { connect } from 'react-redux';
import { actionLogin } from '../../Store/Acoes';

class SignIn extends Component {
  constructor() {
    super();
    this.state = { nome: 'instagram-clone' };
  }

  LoginUsuario = (values) => {
    this.props.login(values);
  };

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <SignInForm login={this.LoginUsuario} />
        <Button title="Registrar" onPress={() => { navigation.navigate('SignUp'); }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2C3E50',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
});

const mapStateToProps = state => ({
  prop: state.reducerPrueba,
});

const mapDispatchToProps = dispatch => ({
  login: (dados) => {
    dispatch(actionLogin(dados));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
