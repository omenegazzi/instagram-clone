import React, { Component } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

const img_logo = require('../imgs/bras_o4.jpg');

class Splash extends Component {
  constructor() {
    super();
    this.state = { nome: 'instagram-clone' };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Grupo Menegazzi Tecnologia Ltda</Text>
        <Image style={styles.logo} source={img_logo}></Image>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(32, 53, 70)',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 50,
    color: 'white',
  },
  logo: {
    width: 128,
    height: 200,
  },
});

export default Splash;
