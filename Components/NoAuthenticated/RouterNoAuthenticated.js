import React from 'react';
import { Text, View, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';
import SignIn from './SignIn';
import SignUp from './SignUp';

const RouterNoAuthenticated = StackNavigator(
  {
    SignIn: { screen: SignIn },
    SignUp: { screen: SignUp },
  },
  {
    navigationOptions: {
      title: 'App Aluguel',
    },
  },
);

export default RouterNoAuthenticated;
