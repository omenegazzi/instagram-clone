import React, { Component } from 'react';
import { StyleSheet, View, Text, Button, TextInput } from 'react-native';
import { connect } from 'react-redux';
import { blur, change } from 'redux-form';
import SignUpForm from './Forms/SignUpForm';
import { actionRegistro, actionCargaImagem, actionLimparImagem } from '../../Store/Acoes';
import SelecionaImagem from '../SelecionaImagem';
import Constantes from '../../Store/Constantes';

class SignUp extends Component {
  componentWillUnmount() {
    this.props.limparImagem();
  }
  registroDeUsuario = (values) => {
    this.props.registro(values);
  };

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <SelecionaImagem
          imagem={this.props.imagem.imagem}
          carga={this.props.cargaImagem}
        />
        <SignUpForm registro={this.registroDeUsuario} imagem={this.props.imagem.imagem} />
        <Button title="Login" onPress={() => { navigation.goBack(); }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#2C3E50',
    paddingHorizontal: 16,
  },
});

const mapStateToProps = state => ({
  numero: state.reducerPrueba,
  imagem: state.reducerImagemSignUp,
});

const mapDispatchToProps = dispatch => ({
  registro: (values) => {
    dispatch(actionRegistro(values));
  },
  cargaImagem: (imagem) => {
    dispatch(actionCargaImagem(imagem));
    dispatch(blur('SignUpForm', 'imagem', Date.now()));
  },
  limparImagem: () => {
    dispatch(actionLimparImagem());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
