import React, { Component } from 'react';
import {
  StyleSheet, View, Text, Image,
  TouchableWithoutFeedback, StatusBar,
  TextInput, SafeAreaView, Keyboard,
  TouchableOpacity, KeyboardAvoidingView,
} from 'react-native';
import SignInForm from './Forms/SignInForm';

const img_logo = require('../imgs/logo_scfa.png');

class Login extends Component {
  constructor() {
    super();
    this.state = { nome: 'instagram-clone' };
  }
  render() {
    const { navigation } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="light-content" />
        <KeyboardAvoidingView behavior="padding" style={styles.container}>
          <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss} >
            <View style={styles.logoContainer}>
              <View style={styles.logoContainer}>
                <Image style={styles.logo} source={img_logo}>
                </Image>
                <Text style={styles.title}> Sistema Controle Fiscalização Acessibilidade </Text>
              </View>
              <View style={styles.infoContainer}>
                <TextInput style={styles.input}
                  underlineColorAndroid="transparent"
                  placeholder="Entre com e-mail"
                  placeholderTextColor="rgba(255,255,255,0.8)"
                  keyboardType="email-address"
                  returnKeyType="next"
                  autoCorrect={false}
                  onSubmitEditing={() => this.refs.txtPassword.focus()}
                />
                <TextInput style={styles.input}
                  underlineColorAndroid="transparent"
                  placeholder="Entre com a senha"
                  placeholderTextColor="rgba(255,255,255,0.8)"
                  returnKeyType="go"
                  secureTextEntry
                  autoCorrect={false}
                  ref={"txtPassword"}
                />
                <View style={styles.buttonContainer}>
                  <TouchableOpacity style={styles.buttonContainer} onPress={() => { navigation.navigate('SignUp'); }}>
                    <Text style={styles.buttonText}> Entrar </Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.buttonContainer}>
                  <TouchableOpacity style={styles.buttonContainer}>
                    <Text style={styles.buttonText}> Registrar </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(32, 53, 70)',
    flex: 1,
    flexDirection: 'column',
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  logo: {
    width: 128,
    height: 56,
  },
  title: {
    color: '#F7C744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9,
  },
  infoContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 250,
    padding: 20,
  },
  input: {
    height: 40,
    backgroundColor: 'rgba(255,255,255,0.2)',
    color: '#FFF',
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  buttonContainer: {
    backgroundColor: '#F7C744',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  buttonText: {
    color: 'rgb(32, 53, 70)',
    fontWeight: 'bold',
    fontSize: 18,
    paddingHorizontal: 10,
  },
});

export default Login;
