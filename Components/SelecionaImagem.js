import React from 'react';
import { Image, View, TouchableOpacity } from 'react-native';
import { ImagePicker, Permissions } from 'expo';

askPermissionsAsync = async () => {
  await Permissions.askAsync(Permissions.CAMERA);
  await Permissions.askAsync(Permissions.CAMERA_ROLL);
  await Permissions.askAsync(Permissions.CONTACTS);
  
  // you would probably do something to verify that permissions
  // are actually granted, but I'm skipping that for brevity
};

const SelecionarImagem = (props) => {
  const selecionarImagem = async () => {
    await this.askPermissionsAsync();
    const result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });
    console.log(result);

    if (!result.cancelled) {
      props.carga(result);
    }
  };

  const radius = { borderRadius: props.radius ? 0 : 80 }; 

  return (
    <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}>
      <TouchableOpacity onPress={selecionarImagem} >
        {props.imagem ? (
          <Image
            source={{ uri: props.imagem.uri }}
            style={{ width: 160, height: 160, ...radius }}
          />
        ) : (
          <Image
            source={require('./imgs/perfil.png')}
            style={{ width: 160, height: 160, ...radius }}
          />
          )}
      </TouchableOpacity>
    </View>
  );
};

export default SelecionarImagem;
