import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { autentication } from './Store/Services/Firebase';
import RouterAuthenticated from './Components/Authenticated/RouterAuthenticated';
import RouterNoAuthenticated from './Components/NoAuthenticated/RouterNoAuthenticated';
import { actionEncerrarSession, actionEstabeleceSession } from './Store/Acoes';

class Selecao extends Component {
  componentDidMount() {
    this.props.autentication();
  }
  render() {
    return (
      <View style={styles.container}>
        {this.props.usuario ? <RouterAuthenticated /> : <RouterNoAuthenticated />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const mapStateToProps = state => ({
  usuario: state.reducerSession,
});

const mapDispatchToProps = dispatch => ({
  autentication: () => {
    autentication.onAuthStateChanged((usuario) => {
      if (usuario) {
        // User is signed in.
        console.log(usuario);
        dispatch(actionEstabeleceSession(usuario));

        // ...
      } else {
        console.log('Não existe sessão');
        dispatch(actionEncerrarSession());
        // User is signed out.
        // ...
      }
    });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Selecao);

